<?php

declare(strict_types=1);

namespace Test\App\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

final class IndexControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = self::createClient();
        $client->request('GET', '/');

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('p', 'Notify user');
        self::assertSelectorTextSame('label[for=form_user_id]', 'User id');
        self::assertSelectorTextSame('label[for=form_user_email]', 'User email');
        self::assertSelectorTextSame('label[for=form_subject]', 'Subject');
        self::assertSelectorTextSame('label[for=form_content]', 'Content');
        self::assertSelectorTextSame('button', 'Submit');
    }

    public function testSendingForm(): void
    {
        $client = self::createClient();
        $crawler = $client->request('GET', '/');

        $form = $crawler
            ->filter('form')
            ->form([
                'form[user_id]' => 1,
                'form[user_email]' => 'user@domain.com',
                'form[subject]' => ':subject:',
                'form[content]' => ':content:',
            ]);

        self::assertEquals('POST', $form->getMethod());
    }
}

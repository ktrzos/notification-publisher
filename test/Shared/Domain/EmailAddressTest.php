<?php

declare(strict_types=1);

namespace Test\Shared\Domain;

use Generator;
use InvalidArgumentException;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use Shared\Domain\EmailAddress;

final class EmailAddressTest extends TestCase
{
    public function testConstructValid(): void
    {
        $value = 'user@domain.com';
        $object = new EmailAddress($value);

        self::assertEquals($value, $object);
        self::assertEquals($value, $object->value);
    }

    #[DataProvider('providerInvalidEmailValues')]
    public function testConstructInvalid(string $invalid): void
    {
        $this->expectException(InvalidArgumentException::class);
        new EmailAddress($invalid);
    }

    /**
     * @return Generator<array{string}>
     */
    public static function providerInvalidEmailValues(): Generator
    {
        yield [''];
        yield ['user'];
        yield ['123'];
        yield ['domain.com'];
    }
}

<?php

declare(strict_types=1);

namespace Test\NotificationPublisher\Application\EventListener;

use NotificationPublisher\Application\Command\SendNotification;
use NotificationPublisher\Application\EventListener\NotificationHandledListener;
use NotificationPublisher\Domain\Entity\UserId;
use NotificationPublisher\Domain\Event\NotificationHandledEvent;
use stdClass;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Event\WorkerMessageHandledEvent;
use Symfony\Component\Notifier\Recipient\Recipient;
use Test\NotificationPublisher\AbstractMessengerTransportTestCase;

final class NotificationHandledListenerTest extends AbstractMessengerTransportTestCase
{
    public function testReceivedSendNotificationMessage(): void
    {
        $message = new SendNotification(
            userId: new UserId(1),
            recipient: new Recipient(email: 'user@domain.com'),
            subject: ':subject:',
            message: ':message:',
        );

        $eventListener = self::getContainer()->get(NotificationHandledListener::class);
        $eventListener(
            event: $this->createEventWithMessage($message),
        );

        $envelopeList = self::getTransport()->getSent();

        self::assertCount(1, $envelopeList);
        self::assertInstanceOf(NotificationHandledEvent::class, $envelopeList[0]->getMessage());
    }

    public function testReceivedOtherMessage(): void
    {
        $eventListener = self::getContainer()->get(NotificationHandledListener::class);
        $eventListener(
            event: $this->createEventWithMessage(new stdClass()),
        );

        $envelopeList = self::getTransport()->getSent();

        self::assertCount(0, $envelopeList);
    }

    private function createEventWithMessage(object $message): WorkerMessageHandledEvent
    {
        return new WorkerMessageHandledEvent(
            envelope: new Envelope($message),
            receiverName: ':receiverName:',
        );
    }
}

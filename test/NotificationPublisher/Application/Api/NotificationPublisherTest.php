<?php

declare(strict_types=1);

namespace Test\NotificationPublisher\Application\Api;

use NotificationPublisher\Application\Api\NotificationPublisher;
use NotificationPublisher\Application\Command\SendNotification;
use NotificationPublisher\Domain\Entity\UserId;
use Shared\Domain\EmailAddress;
use Symfony\Component\Messenger\MessageBusInterface;
use Test\NotificationPublisher\AbstractMessengerTransportTestCase;

final class NotificationPublisherTest extends AbstractMessengerTransportTestCase
{
    public function testPublish(): void
    {
        $container = self::getContainer();

        $message_bus = $container->get(MessageBusInterface::class);

        $publisher = new NotificationPublisher($message_bus);
        $publisher->publish(
            userId: new UserId(1),
            email: new EmailAddress('user@domain.com'),
            subject: ':subject:',
            content: ':content:',
        );

        $envelopeList = self::getTransport()->getSent();

        self::assertCount(1, $envelopeList);
        self::assertInstanceOf(SendNotification::class, $envelopeList[0]->getMessage());
    }
}

<?php

declare(strict_types=1);

namespace Test\NotificationPublisher\Application\CommandHandler;

use NotificationPublisher\Application\Command\SendNotification;
use NotificationPublisher\Application\Command\SendNotificationHandler;
use NotificationPublisher\Domain\Entity\UserId;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Notifier\Recipient\Recipient;
use Webmozart\Assert\Assert;

final class SendNotificationHandlerTest extends KernelTestCase
{
    public function testInvoke(): void
    {
        $container = self::getContainer();

        $handler = $container->get(SendNotificationHandler::class);
        $handler(
            new SendNotification(
                userId: new UserId(1),
                recipient: new Recipient(email: 'user@domain.com'),
                subject: ':subject:',
                message: ':message:',
            ),
        );

        self::assertNotificationCount(1);

        $message = self::getNotifierMessage();
        Assert::notNull($message);

        self::assertEquals(':subject:', $message->getSubject());
    }
}

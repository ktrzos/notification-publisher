<?php

declare(strict_types=1);

namespace Test\NotificationPublisher\Application\CommandHandler;

use Doctrine\ORM\EntityManagerInterface;
use NotificationPublisher\Application\Command\LogNotificationHandler;
use NotificationPublisher\Domain\Entity\LoggedNotification;
use NotificationPublisher\Domain\Entity\UserId;
use NotificationPublisher\Domain\Event\NotificationHandledEvent;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class LogNotificationHandlerTest extends KernelTestCase
{
    public function testInvoke(): void
    {
        self::assertCountLoggedNotifications(0);

        $handler = self::getContainer()->get(LogNotificationHandler::class);
        $handler(
            new NotificationHandledEvent(
                userId: new UserId(1),
                subject: ':subject:',
                message: ':message:',
            ),
        );

        self::assertCountLoggedNotifications(1);
    }

    private static function assertCountLoggedNotifications(int $expectedCount): void
    {
        $entityList = self::getContainer()->get(EntityManagerInterface::class)
            ->getRepository(LoggedNotification::class)
            ->findAll();

        self::assertCount($expectedCount, $entityList);
    }
}

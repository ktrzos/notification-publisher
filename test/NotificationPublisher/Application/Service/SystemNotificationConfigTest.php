<?php

declare(strict_types=1);

namespace Test\NotificationPublisher\Application\Service;

use NotificationPublisher\Application\Service\SystemNotificationConfig;
use PHPUnit\Framework\TestCase;

final class SystemNotificationConfigTest extends TestCase
{
    public function testBasic(): void
    {
        $config = new SystemNotificationConfig(
            appEmailAddress: 'user@domain.com',
            notifierChannels: ['email'],
        );

        self::assertEquals('user@domain.com', $config->getAppEmailAddress()->value);
        self::assertEquals(['email'], $config->getNotifierChannels());
    }
}

<?php

declare(strict_types=1);

namespace Test\NotificationPublisher;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Messenger\Transport\InMemoryTransport;
use Webmozart\Assert\Assert;

abstract class AbstractMessengerTransportTestCase extends KernelTestCase
{
    public static function getTransport(): InMemoryTransport
    {
        /** @var InMemoryTransport $transport */
        $transport = self::getContainer()->get('messenger.transport.async_notifications');

        Assert::isInstanceOf($transport, InMemoryTransport::class);

        return $transport;
    }
}

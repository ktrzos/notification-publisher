# Notification Publisher
A service that accepts the necessary information and sends a notification to customers via different channels.

## Configuration: 
- Notifications: `config/packages/notifier.yaml`
- QueryBus: `config/packages/messenger.yaml`
- Rate limiter: `config/packages/rate_limiter.yaml`

## How to run
- Copy `.env` as `.env.local` and fill all environment variables
  - APP_EMAIL_ADDR
  - DATABASE_URL
  - MAILER_DSN
  - DISCORD_DSN
  - SLACK_DSN
- Run Dockerized environment: `docker/start.sh`
- Create database: 
  - `bin/symfony doctrine:database:create`
  - `bin/symfony doctrine:migrations:migrate --no-interaction`
- Run Symfony Messenger worker: `bin/symfony messenger:consume async_notifications -vv`
- Project should be available through https://localhost

## 
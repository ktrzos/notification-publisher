<?php

declare(strict_types=1);

namespace App\Controller;

use NotificationPublisher\Application\Api\NotificationPublisher;
use NotificationPublisher\Domain\Entity\UserId;
use Shared\Domain\EmailAddress;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Webmozart\Assert\Assert;

final class IndexController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(Request $request, NotificationPublisher $notificationPublisher): Response
    {
        // @TODO: Change form builder onto dedicated Form class
        // @TODO: Change form input onto dedicated data class
        $formBuilder = $this->createFormBuilder([
            'user_id' => 1,
            'subject' => 'Notification subject',
            'content' => 'Notification content',
        ]);
        $formBuilder->setMethod('POST');
        $formBuilder->add('user_id', TextType::class);
        $formBuilder->add('user_email', EmailType::class);
        $formBuilder->add('subject', TextType::class);
        $formBuilder->add('content', TextareaType::class);
        $formBuilder->add('submit', SubmitType::class);

        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            Assert::isArray($data);

            $notificationPublisher->publish(
                userId: new UserId((int) $data['user_id']),
                email: new EmailAddress($data['user_email']),
                subject: (string) $data['subject'],
                content: (string) $data['content'],
            );

            $this->addFlash('success', 'Notification sent.');
        }

        return $this->render('base.html.twig', [
            'form' => $form,
        ]);
    }
}

<?php

declare(strict_types=1);

namespace NotificationPublisher\Domain\Event;

use Carbon\Carbon;
use DateTimeInterface;
use NotificationPublisher\Domain\Entity\UserId;

final readonly class NotificationHandledEvent
{
    public DateTimeInterface $handleDate;

    public function __construct(
        public UserId $userId,
        public string $subject,
        public string $message,
    ) {
        $this->handleDate = Carbon::now();
    }
}

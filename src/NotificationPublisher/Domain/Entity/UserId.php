<?php

declare(strict_types=1);

namespace NotificationPublisher\Domain\Entity;

final readonly class UserId
{
    public function __construct(public int $id)
    {
    }
}

<?php

declare(strict_types=1);

namespace NotificationPublisher\Domain\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Ulid;

#[ORM\Entity]
class LoggedNotification
{
    #[ORM\Id]
    #[ORM\Column(type: 'ulid')]
    private Ulid $id;

    #[ORM\Column(type: 'integer')]
    private readonly int $userId;

    #[ORM\Column(type: 'datetime')]
    private DateTimeInterface $creationDate;

    #[ORM\Column(type: 'string', length: 500)]
    private readonly string $subject;

    #[ORM\Column(type: 'text')]
    private readonly string $content;

    public function __construct(
        UserId $userId,
        string $subject,
        string $content,
        DateTimeInterface $handleDate,
    ) {
        $this->id = new Ulid();
        $this->creationDate = $handleDate;
        $this->userId = $userId->id;
        $this->subject = $subject;
        $this->content = $content;
    }
}

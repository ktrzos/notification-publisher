<?php

declare(strict_types=1);

namespace NotificationPublisher\Application\Notification;

use Symfony\Component\Mime\Email;
use Symfony\Component\Notifier\Message\ChatMessage;
use Symfony\Component\Notifier\Message\EmailMessage;
use Symfony\Component\Notifier\Notification\ChatNotificationInterface;
use Symfony\Component\Notifier\Notification\EmailNotificationInterface;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\EmailRecipientInterface;
use Symfony\Component\Notifier\Recipient\RecipientInterface;

final class SystemNotification extends Notification implements EmailNotificationInterface, ChatNotificationInterface
{
    public function __construct(public readonly string $from, string $subject = '', array $channels = [])
    {
        parent::__construct($subject, $channels);
    }

    public function asEmailMessage(EmailRecipientInterface $recipient, string $transport = null): ?EmailMessage
    {
        $email = (new Email())
            ->from($this->from)
            ->to($recipient->getEmail())
            ->subject($this->getSubject())
            ->text($this->getContent());

        return new EmailMessage($email);
    }

    public function asChatMessage(RecipientInterface $recipient, string $transport = null): ?ChatMessage
    {
        return ChatMessage::fromNotification($this);
    }
}

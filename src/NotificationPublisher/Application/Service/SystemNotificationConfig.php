<?php

declare(strict_types=1);

namespace NotificationPublisher\Application\Service;

use Shared\Domain\EmailAddress;

final readonly class SystemNotificationConfig
{
    private EmailAddress $appEmailAddress;

    /**
     * @param string[] $notifierChannels
     */
    public function __construct(
        string $appEmailAddress,
        private array $notifierChannels,
    ) {
        $this->appEmailAddress = new EmailAddress($appEmailAddress);
    }

    public function getAppEmailAddress(): EmailAddress
    {
        return $this->appEmailAddress;
    }

    /**
     * @return string[]
     */
    public function getNotifierChannels(): array
    {
        return $this->notifierChannels;
    }
}

<?php

declare(strict_types=1);

namespace NotificationPublisher\Application\Command;

use NotificationPublisher\Domain\Entity\UserId;
use Symfony\Component\Notifier\Recipient\RecipientInterface;

final readonly class SendNotification
{
    public function __construct(
        public UserId $userId,
        public RecipientInterface $recipient,
        public string $subject,
        public string $message,
    ) {
    }
}

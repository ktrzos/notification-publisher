<?php

declare(strict_types=1);

namespace NotificationPublisher\Application\Command;

use NotificationPublisher\Application\Notification\SystemNotification;
use NotificationPublisher\Application\Service\SystemNotificationConfig;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Notifier\NotifierInterface;

#[AsMessageHandler]
final readonly class SendNotificationHandler
{
    public function __construct(
        private NotifierInterface $notifier,
        private SystemNotificationConfig $config,
    ) {
    }

    public function __invoke(SendNotification $command): void
    {
        $notification = new SystemNotification(
            from: $this->config->getAppEmailAddress()->value,
            channels: $this->config->getNotifierChannels(),
        );
        $notification->subject($command->subject);
        $notification->content($command->message);

        $this->notifier->send($notification, $command->recipient);
    }
}

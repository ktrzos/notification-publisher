<?php

declare(strict_types=1);

namespace NotificationPublisher\Application\Command;

use Doctrine\ORM\EntityManagerInterface;
use NotificationPublisher\Domain\Entity\LoggedNotification;
use NotificationPublisher\Domain\Event\NotificationHandledEvent;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class LogNotificationHandler
{
    public function __construct(private EntityManagerInterface $em)
    {
    }

    public function __invoke(NotificationHandledEvent $command): void
    {
        $entity = new LoggedNotification(
            userId: $command->userId,
            subject: $command->subject,
            content: $command->message,
            handleDate: $command->handleDate,
        );

        $this->em->persist($entity);
        $this->em->flush();
    }
}

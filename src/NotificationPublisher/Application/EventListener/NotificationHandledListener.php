<?php

declare(strict_types=1);

namespace NotificationPublisher\Application\EventListener;

use NotificationPublisher\Application\Command\SendNotification;
use NotificationPublisher\Domain\Event\NotificationHandledEvent;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\Messenger\Event\WorkerMessageHandledEvent;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsEventListener]
final readonly class NotificationHandledListener
{
    public function __construct(private MessageBusInterface $messageBus)
    {
    }

    public function __invoke(WorkerMessageHandledEvent $event): void
    {
        $message = $event->getEnvelope()->getMessage();

        if (false === ($message instanceof SendNotification)) {
            return;
        }

        $this->messageBus->dispatch(
            new NotificationHandledEvent(
                userId: $message->userId,
                subject: $message->subject,
                message: $message->message,
            ),
        );
    }
}

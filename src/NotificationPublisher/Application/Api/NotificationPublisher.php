<?php

declare(strict_types=1);

namespace NotificationPublisher\Application\Api;

use NotificationPublisher\Application\Command\SendNotification;
use NotificationPublisher\Domain\Entity\UserId;
use Shared\Domain\EmailAddress;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Notifier\Recipient\Recipient;

final readonly class NotificationPublisher
{
    public function __construct(
        private MessageBusInterface $messageBus,
    ) {
    }

    public function publish(
        UserId $userId,
        EmailAddress $email,
        string $subject,
        string $content,
    ): void {
        $this->messageBus->dispatch(
            new SendNotification(
                userId: $userId,
                recipient: new Recipient($email->value),
                subject: $subject,
                message: $content,
            ),
        );
    }
}

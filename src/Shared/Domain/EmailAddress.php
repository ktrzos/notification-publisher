<?php

declare(strict_types=1);

namespace Shared\Domain;

use InvalidArgumentException;
use Stringable;

final readonly class EmailAddress implements Stringable
{
    public string $value;

    public function __construct(?string $value)
    {
        if (null === $value
            || '' === $value
            || false === filter_var($value, FILTER_VALIDATE_EMAIL)
        ) {
            throw new InvalidArgumentException('Invalid e-mail address given to the domain object!');
        }

        $this->value = $value;
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
